#!/bin/bash

set -eu

USAGE="Usage: $(basename $0) [MIRRORBITS_ARGS] [LOGS_ARGS]
Quickly check for errors in the logs for each mirror.

Example: $(basename $0) -p 3390
"

MIRRORS=$(mirrorbits "$@" list | awk '{print $1}' | grep -iv "^identifier\b")

for m in $MIRRORS; do
    echo $m
    echo =========================
    mirrorbits "$@" logs -l 20 $m | grep -i error || :
    echo
done
