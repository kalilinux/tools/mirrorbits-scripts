#!/bin/bash

set -eu

USAGE="Usage: $(basename $0) ID [REDIS_ARGS]
Remove a mirror in the redis database, thus by-passing mirrorbits.
Maybe best to stop mirrorbits before?
Use at your own risk.

Example: $(basename $0) 76 -p 6380
"

if [ $# -eq 0 ]; then
    echo "$USAGE" >&2
    exit 1
fi

ID=$1; shift

if ! echo "$ID" | grep -q '^[0-9]\+$'; then
    echo "$USAGE" >&2
    exit 1
fi

# Find which redis cli to use
REDIS_CLI=redis-cli
for x in valkey redict keydb; do
    if command -v $x-cli 2>&1 >/dev/null; then
        REDIS_CLI=$x-cli
        break
    fi
done
REDIS="$REDIS_CLI $@"

# Find mirror per ID
MIRROR=$($REDIS --raw hgetall MIRRORS | grep -A1 "^${ID}$" | tail -1)
if [ -z "$MIRROR" ]; then
    echo "Couldn't find mirror with ID $ID! Aborting!" >&2
    exit 1
else
    echo "ID $ID = $MIRROR"
fi

buf=$($REDIS smembers MIRRORFILES_${ID})
if [ "$buf" != "" ]; then
    echo "MIRRORFILES_${ID} is not empty! Aborting!" >&2
    exit 1
fi

# This one might take a while, use "--count 1000" for redis >= 7.2
if false; then
buf=$($REDIS --scan --pattern "FILEINFO_${ID}_*")
if [ "$buf" != "" ]; then
    echo "Keys with pattern FILEINFO_${ID}_* exist! Aborting!" >&2
    exit 1
fi
fi

# Proceed to delete the mirror
echo "Good to go, about to remove mirror."
echo "Press Enter to confirm, Ctrl-C to abort..."
read -r

set -x

$REDIS del MIRROR_${ID}
$REDIS del MIRRORFILES_${ID}
$REDIS del MIRRORFILESTMP_${ID}
$REDIS del HANDLEDFILES_${ID}
$REDIS del SCANNING_${ID}
$REDIS del MIRRORLOGS_${ID}

$REDIS hdel MIRRORS ${ID}
